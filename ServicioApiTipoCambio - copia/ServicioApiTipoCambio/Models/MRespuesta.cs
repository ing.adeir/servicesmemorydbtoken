﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioApiTipoCambio.Models
{
    public class MRespuesta
    {
        public Boolean bResultado { get; set; }
        public string sMensaje { get; set; }
    }
}
