﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioApiTipoCambio.Models
{
    public class MTipoCambioRequest
    {
        public double mMonto { get; set; }
        public string sNomMonedaOrigen { get; set; }
        public string sNomMonedaDestino { get; set; }
        public double mTipoDeCambio { get; set; }
    }
}
