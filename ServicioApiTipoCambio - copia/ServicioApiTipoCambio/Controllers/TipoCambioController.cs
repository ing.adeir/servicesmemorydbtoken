﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicioApiTipoCambio.Models;

namespace ServicioApiTipoCambio.Controllers
{
    [Route("api/TipoCambio")]
    [ApiController]
    public class TipoCambioController : ControllerBase
    {

        private readonly ApiContext apiContext;

        public TipoCambioController(ApiContext apiContext)
        {
            this.apiContext = apiContext;
        }


        // GET api/TipoCambio/ObtenerTipoCambio
        [HttpGet]
        [Route("ObtenerTipoCambio")]
        public async Task<IActionResult> ObtenerTipoCambio()
        {
            List<MTipoCambioResponse> _response = apiContext.GetTipoCambio();
            return Ok(_response);
        }

        ///api/TipoCambio/GrabarTipoCambio
        [HttpPost]
        [Route("GrabarTipoCambio")]
        public async Task<IActionResult> GrabarTipoCambio([FromBody] MTipoCambioRequest _request)
        {
            var resultado = apiContext.AddTipoCambio(_request);
            return Ok(resultado);
        }





    }
}