﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServicioApiTipoCambio.Models;

namespace ServicioApiTipoCambio
{
    public class ApiContext : DbContext
    {
        public DbSet<MTipoCambioResponse> dsTipoCambios { get; set; }

        public ApiContext(DbContextOptions options) : base(options)
        {
            LoadTipoCambios();
        }

        public void LoadTipoCambios()
        {
            MTipoCambioResponse _MTipoCambio = new MTipoCambioResponse() { mMonto = 15.2, mMontoConTipoCambio = 10.2, mTipoCambio=12.4 , sMonedaOrigen="Dolar", sMonedaDestino="Soles" };
            dsTipoCambios.Add(_MTipoCambio);
            _MTipoCambio = new MTipoCambioResponse() { mMonto = 15.1, mMontoConTipoCambio = 10.2, mTipoCambio = 12.4, sMonedaOrigen = "Euro", sMonedaDestino = "Soles" };
            dsTipoCambios.Add(_MTipoCambio);
            _MTipoCambio = new MTipoCambioResponse() { mMonto = 15.1, mMontoConTipoCambio = 10.2, mTipoCambio = 12.4, sMonedaOrigen = "Peso Mexicano", sMonedaDestino = "Soles" };
            dsTipoCambios.Add(_MTipoCambio);
        }

        public void RegistrarTipoCambio(MTipoCambioRequest _request)
        {
            double mMontoConTipoCambio = _request.mMonto * _request.mTipoDeCambio;
            MTipoCambioResponse _MTipoCambio = new MTipoCambioResponse() { mMonto = _request.mMonto,
                mMontoConTipoCambio = mMontoConTipoCambio, mTipoCambio=_request.mTipoDeCambio,
                sMonedaOrigen= _request.sNomMonedaOrigen, sMonedaDestino = _request.sNomMonedaDestino
            };
            dsTipoCambios.Add(_MTipoCambio);
        }

        public List<MTipoCambioResponse> GetTipoCambio()
        {
            return dsTipoCambios.Local.ToList<MTipoCambioResponse>();
        }

        public MRespuesta AddTipoCambio(MTipoCambioRequest _request)
        {
            MRespuesta rpta = new MRespuesta();
            try
            {
                RegistrarTipoCambio(_request);
                rpta.bResultado = true;
                rpta.sMensaje = "Grabado Exitoso.";
            }
            catch (Exception ex)
            {
                rpta.bResultado = false;
                rpta.sMensaje = "Error:" +ex.ToString();
            }
            
            return rpta;
        }


    }
}
