﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServicioApiTipoCambio.Models;

namespace ServicioApiTipoCambio
{
    public class ApiContext : DbContext
    {
        public DbSet<MTipoCambioResponse> dsTipoCambios { get; set; }

        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
           
        }

      
    }
}
