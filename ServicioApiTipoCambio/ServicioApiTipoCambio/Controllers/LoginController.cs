﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ServicioApiTipoCambio.Models;

namespace ServicioApiTipoCambio.Controllers
{
    [Route("api/logeo")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration configuration;

        public LoginController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        // POST: api/Login
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLogin usuarioLogin)
        {
            var _userInfo = await AutenticarUsuarioAsync(usuarioLogin.sUsuario, usuarioLogin.sPassword);
            if (_userInfo != null)
            {
                return Ok(new { token = GenerarTokenJWT(_userInfo) });
            }
            else
            {
                return Unauthorized();
            }
        }

        // validacion del usuario
        private async Task<UserInfo> AutenticarUsuarioAsync(string usuario, string password)
        {
            if (usuario =="adeir" && password =="adeir123")
            {
                return new UserInfo()
                {
                    // Id del User en el Sistema de Informacion
                    gId = new Guid("B5D233F0-6EC2-4950-8CD7-F44D16EC878F"),
                    sNombre = "Adeir Fausto",
                    sApellidos = "Modragon Aguirre",
                    sEmail = "ing.adeir@gmail.com",
                    sRol = "Administrador"
                };

            }
            else
            {
                return null;
            }

        }

        // generacion del token
        private string GenerarTokenJWT(UserInfo usuarioInfo)
        {
            // HEADER //
            var _symmetricSecurityKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(configuration["JWT:ClaveSecreta"])
                );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );
            var _Header = new JwtHeader(_signingCredentials);

            // CLAIMS //
            var _Claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, usuarioInfo.gId.ToString()),
                new Claim("nombre", usuarioInfo.sNombre),
                new Claim("apellidos", usuarioInfo.sApellidos),
                new Claim(JwtRegisteredClaimNames.Email, usuarioInfo.sEmail),
                new Claim(ClaimTypes.Role, usuarioInfo.sRol)
            };

            // PAYLOAD //
            var _Payload = new JwtPayload(
                    issuer: configuration["JWT:Issuer"],
                    audience: configuration["JWT:Audience"],
                    claims: _Claims,
                    notBefore: DateTime.UtcNow,
                    // vence a la 24 horas.
                    expires: DateTime.UtcNow.AddHours(24)
                );

            // GENERAMOS EL TOKEN //
            var _Token = new JwtSecurityToken(
                    _Header,
                    _Payload
                );

            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }





    }
}