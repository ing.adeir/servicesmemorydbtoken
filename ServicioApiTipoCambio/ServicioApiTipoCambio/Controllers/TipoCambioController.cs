﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicioApiTipoCambio.Models;

namespace ServicioApiTipoCambio.Controllers
{
    [Route("api/TipoCambio")]
    [ApiController]
    public class TipoCambioController : Controller
    {

        private ApiContext apiContext;

        public TipoCambioController(ApiContext apiContext)
        {
            this.apiContext = apiContext;
            //if(!this.apiContext.dsTipoCambios.Any())
            //{
            //    this.apiContext.dsTipoCambios.Add(new MTipoCambioResponse() { mMonto = 15.1, mMontoConTipoCambio = 10.2, mTipoCambio = 12.4, sMonedaOrigen = "Euro", sMonedaDestino = "Soles" } );
            //    this.apiContext.SaveChanges();
            //}
        }

        ///api/TipoCambio/ObtenerTipoCambio
        [HttpGet]
        [Route("ObtenerTipoCambio")]
        [Authorize] // SOLO USUARIOS AUTENTICADOS
        public IEnumerable<MTipoCambioResponse> ObtenerTipoCambio()
        {
            return apiContext.dsTipoCambios;
        }

        ///api/TipoCambio/GrabarTipoCambio
        [HttpPost]
        [Route("GrabarTipoCambio")]
        public MRespuesta GrabarTipoCambio(MTipoCambioRequest _request)
        {

            double mConTipoDeCambio = Math.Round((_request.mMonto * _request.mTipoDeCambio),2);
            MTipoCambioResponse _response = new MTipoCambioResponse() {
                mMonto=_request.mMonto, mMontoConTipoCambio=mConTipoDeCambio, mTipoCambio=_request.mTipoDeCambio,
                sMonedaDestino = _request.sNomMonedaDestino, sMonedaOrigen=_request.sNomMonedaOrigen
            };
            MRespuesta _MRespuesta = new MRespuesta();
            try
            {
                apiContext.dsTipoCambios.Add(_response);
                apiContext.SaveChanges();
                _MRespuesta.bResultado = true;
                _MRespuesta.sMensaje = "Registro Grabado con Exito";
            }
            catch (Exception ex)
            {
                _MRespuesta.bResultado = false;
                _MRespuesta.sMensaje = "Error: " + ex.ToString();
            }
            return _MRespuesta;
        }





    }
}