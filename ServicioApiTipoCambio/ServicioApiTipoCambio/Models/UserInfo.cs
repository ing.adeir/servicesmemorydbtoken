﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioApiTipoCambio.Models
{
    public class UserInfo
    {
        public Guid gId { get; set; }
        public string sNombre { get; set; }
        public string sApellidos { get; set; }
        public string sEmail { get; set; }
        public string sRol { get; set; }
    }
}
