﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioApiTipoCambio.Models
{
    public class MTipoCambioResponse
    {
        public int Id { get; set; }
        public double mMonto { get; set; }
        public double mMontoConTipoCambio { get; set; }
        public double mTipoCambio { get; set; }
        public string sMonedaOrigen { get; set; }
        public string sMonedaDestino { get; set; }
    }
}
