﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioApiTipoCambio.Models
{
    public class UserLogin
    {
        public string sUsuario { get; set; }
        public string sPassword { get; set; }
    }
}
